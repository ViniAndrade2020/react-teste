import React, { Component } from 'react'
import { View, Text, StyleSheet,Image } from 'react-native'

import Pedra from '../componentes/Pedra'
import Papel from '../componentes/Papel'
import Tesoura from '../componentes/Tesoura'

import Randomico from '../componentes/Randomico'
//import Venceu_Perdeu from '../componentes/venceu_perdeu'


export default class TelaInicial extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.fundo}>
                    <Text style={styles.titulo}>Jokempô</Text>
                </View>
                <View style = {styles.venceu_perdeu}>
                    {/*<Venceu_Perdeu >*/}
                    <Text style = {styles.venceu_perdeu}>Você Perdeu!😭</Text>
                </View>
                <View style={styles.maoOponente}>
                    <Randomico style={styles.randomico}/>
                </View>
                <View style= {styles.botoes}>
                    <Image  style ={styles.imagem} source={require('../Imagens/pedra.jpg')} />
                    <Pedra  />
                    <Image  style ={styles.imagem} source={require('../Imagens/papel.jpg')} />
                    <Papel />
                    <Image  style ={styles.imagem} source={require('../Imagens/tesoura.jpg')} />
                    <Tesoura />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E0FFFF'
    },
    fundo: {
        flex: 1
    },
    titulo: {
        flex: 1,
        flexDirection: 'row',
        fontSize: 80,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        marginTop: 60
    },
    botoes: {
        flex: 1,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
    },
    imagem:{
        width: 40,
        height: 50,
        borderRadius: 10
    },
    maoOponente:{
        flex: 1,
       alignItems: 'center'
    },
    venceu_perdeu:{
        flex: 1,
        alignItems: 'center',
        fontSize: 40
    }
})