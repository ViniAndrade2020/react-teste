import React, { Component } from 'react'
import { render } from 'react-dom'
import { View, Image } from 'react-native'

const randomico = props => {
    const max = 150
    const min = 0
    const delta = max - min + 1
    const aleatorio = parseInt(Math.random() * delta) + min
    const tipo = ''

    if (aleatorio >= 0 && aleatorio <= 50  ) {
        return (
            <View>
                <Image style = {{width: 80,height: 100 , borderRadius: 10}} source={require('../Imagens/pedra.jpg')} />
            </View>
        )
        tipo = 'Pedra'
   } else if (aleatorio >= 51 && aleatorio <= 100) {
        return (
            <View>
                <Image style = {{width: 80,height: 100, borderRadius: 10}} source={require('../Imagens/papel.jpg')} />
            </View>
        )
        tipo = 'Papel'
    } else if (aleatorio >= 101 && aleatorio <= 150) {
        return (
            <View>
                <Image style = {{width: 80,height: 100 ,borderRadius: 10}} source={require('../Imagens/tesoura.jpg')} />
            </View>
        )
        tipo = 'Tesoura'
    }

}

export default randomico